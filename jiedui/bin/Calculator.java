public class Calculator {
	String a = new String();
	DataStack datastack;
	CharStack charstack;
	void Getstack(){ //定义栈的大小
		datastack = new DataStack(a.length());
		datastack.getStack();
		charstack = new CharStack(a.length());
		charstack.getStack();
	}
	Rational manage() {
		int i=0;
		Rational r = new Rational();
		double d;
		while(true) {
			if(i>=a.length())
				break;
			char c = a.charAt(i); //看第i处的内容
			if(c>='0' && c<='9') {//从字符串中检测数字
				String temp = String.valueOf(c);
				String s = new String();
				s = s+temp;
				i++;
				if(i>=a.length()) {
					d = Double.parseDouble(s); //将字符串转换为double型
					r.setNumerator(d);
					datastack.push(r);
					break;
				}
				c = a.charAt(i);
				while(c>='0' && c<='9') {
					temp = String.valueOf(c);
					s = s+temp;
					i++;
					if(i>=a.length())
						break;
					c = a.charAt(i);
				}
				d = Double.parseDouble(s);
				r.setNumerator(d);
				datastack.push(r);
				continue;
			}
			else if(c=='*' || c=='/') {//从字符串中检测运算符
				while(true) {
					if(charstack.top==-1) {//如果符号栈顶无元素，入栈。
						charstack.push(c);
						break;
					}
					if(charstack.a[charstack.top]=='+' || charstack.a[charstack.top]=='-') {//如果符号栈顶是+或-，入栈。
						charstack.push(c);
						break;
					}
					else if((charstack.a[charstack.top]=='*' || charstack.a[charstack.top]=='/') && (datastack.top>=1)) {//如果符号栈顶是*或/，并且栈内元素大于等于1，计算。
						calculate(charstack.a[charstack.top]);
						continue;
					}
					else {
						charstack.push(c);
						break;
					}
				}
			}
			else if(c=='+' || c=='-') {
				while(true) {
					if(charstack.top==-1) {//如果符号栈顶无元素，入栈。
						charstack.push(c);
						break;
					}						
					else if((charstack.a[charstack.top]=='+' || charstack.a[charstack.top]=='-' || charstack.a[charstack.top]=='*' || charstack.a[charstack.top]=='/') && (datastack.top>=1)) {
						calculate(charstack.a[charstack.top]);//计算。
						continue;
					}
					else {
						charstack.push(c);
						break;
					}
				}
				
			}
			else if(c=='(') {//如果符号是(,入栈。
				charstack.push(c);
			}
			else if(c==')') {//如果符号是),栈内符号依次计算。
				while(true) {
					if(charstack.a[charstack.top]=='(') {
						charstack.pop();
						break;
					}
					else {
						calculate(charstack.a[charstack.top]);
					}
				}
			}
			i++;
		}
		while(true) {
			if(datastack.top>=1)
				calculate(charstack.a[charstack.top]);
			else
				break;
		}
		return datastack.pop();
	}
	void calculate(char c) {
		Rational a,b;
		charstack.pop();
		b = datastack.pop();
		a = datastack.pop();
		switch(c) {
			case '+':
				datastack.push(a.add(b));
				break;
			case '-':
				datastack.push(a.sub(b));
				break;
			case '*':
				datastack.push(a.muti(b));
				break;
			case '/':
				datastack.push(a.div(b));
				break;
		}
	}
	
}

