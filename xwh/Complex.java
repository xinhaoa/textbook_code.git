/**
 @author XWH20175126
 @date 2019/5/2 21:28
 */
public class Complex{
    private double a;
    private double b;

    public Complex(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public static double getRealPart(double a) {
        return a;
    }

    public static double getImagePart(double b) {
        return b;
    }

    public Complex ComplexAdd(Complex c) {
        return new Complex(a + c.a, b + c.b);
    }
    public Complex ComplexSub(Complex c) {
        return new Complex(a - c.a, b - c.b);
    }
    public Complex ComplexMulti(Complex c) {
        return new Complex(a * c.a - b * c.b, a * c.b + b * c.a);
    }
    public Complex ComplexDiv(Complex c) {
        return new Complex((a * c.b + b * c.a)/(c.b * c.b + c.a * c.a), (b * c.b + a * c.a)/(c.b * c.b + c.a * c.a));
    }

    public String toString() {
        String s = " ";
        if (b > 0) {
            s =  a + "+" + b + "i";
        }
        if (b == 0) {
            s =  a + "";
        }
        if (b < 0) {
            s = a + " " + b + "i";
        }
        return s;
    }
}
