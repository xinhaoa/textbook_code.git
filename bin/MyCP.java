import java.io.*;
public class MyCP {
    public static void main(String[] args) throws IOException {
        String d=args[0];//获得第一个参数
        String File1=args[1];//获得第二个参数：文件名
        String File2=args[2];//获得第三个参数：文件名
        File source = new File(File1);//读取的文件
        File target = new File(File2);//写入的文件

        if(d.equals("-tx")){
            FileReader reader = new FileReader(source);
            FileWriter writer = new FileWriter(target);
            tx(reader, writer);
        }

        if(d.equals("-xt")){
            FileReader reader = new FileReader(target);
            FileWriter writer = new FileWriter(source);
            xt(reader, writer);
        }
    }
    //十转二
    public static void tx(FileReader reader, FileWriter writer) {
        try (FileReader In = reader; FileWriter Out = writer) {
            char[] number = new char[1];
            String result = null;
            String two = null;
            while ((In.read(number)) != -1) {//读取一个十进制数字
                int number1 = (int) number[0];//字符到数字强制转换
                result = "";
                two = Integer.toBinaryString(number1);
                while (two.length() < 4) {//十进制单个数字的范围为0-9，对应二进制表示范围为0000-1001，为下次转换为十进制方便，将其统一补充为四个数字
                    result = '0' + result;
                }
            }
            result = result + two;
            Out.write(result);//FileWriter的write()方法保存得到的二进制到文件中
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }


    //二转十
    public static void xt(FileReader reader, FileWriter writer) {
        try (FileReader In = reader; FileWriter Out = writer) {
            char[] number = new char[4];
            char ch;
            while((In.read(number))!=-1) {
                String result="";
                In.read();
                for(int i=0;i<4;i++){
                    result=result+number[i];
                }
                int ten=Integer.valueOf(result,2);//Integer.valueOf()方法将二进制转为十进制
                ch = (char)ten;
                Out.write(ch);//FileWriter的write()方法保存得到的十进制
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

    }

}

